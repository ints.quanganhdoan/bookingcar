import changeAvatarReducer, { changeAvatar } from './department/changeAvatarReducer';

/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from '@reduxjs/toolkit';

import { InjectedReducersType } from 'utils/types/injector-typings';
import loginReducer from './auth/loginReducer';
import listCarReducer from './department/listCarReducer';
import detailsCarReducer from './department/detailsCarReducer';
import changeNameReducer from './department/changeNameReducer';
import changePassWordReducer from './department/changePassWordReducer';
import orderCarReducer from './department/orderCarReducer';
import listWaitAccessReducer from './department/listWaitAccessReducer';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export const rootReducers = {
  auth: loginReducer,
  listDriver: listCarReducer ,
  detailsDriver: detailsCarReducer,
  changeFullName: changeNameReducer,
  changeAvatar: changeAvatarReducer,
  changePassword: changePassWordReducer,
  orderCar: orderCarReducer,
  listWaitAccess: listWaitAccessReducer,
};

export function createReducer(injectedReducers: InjectedReducersType = {}) {
  // Initially we don't have any injectedReducers, so returning identity function to avoid the error
  if (Object.keys(injectedReducers).length === 0) {
    return (state: any) => state;
  } else {
    return combineReducers({
      ...injectedReducers,
    });
  }
}

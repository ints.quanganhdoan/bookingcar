import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import changeUserInforApi from 'app/axios/api/changeUserInforApi';

interface UserInforState {
    isLoading: boolean;
    error: string | undefined;
    refreshToken: string | undefined;
    accessToken: string | undefined;
    authenticated: boolean;
}


const initialState: UserInforState = {
    isLoading: false,
    error: undefined,
    accessToken: undefined,
    refreshToken: undefined,
    authenticated: false,
}


export const changeFullName = createAsyncThunk(
    'change/fullname',
    async(payload: {accessToken: string; fullname: string}, thunkAPI) => {
      const { accessToken, fullname } = payload;
        try{
            const response = await axios.patch(changeUserInforApi.updateName, {fullname}, {
              headers: {
                "Authorization": `Bearer ${accessToken}`
              }
            });
            return response.data;
        } catch (error: any){
            return thunkAPI.rejectWithValue({ error: error.message });
        }
    }
) 

const changeFullNameSlice = createSlice({
    name: 'changeName',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
          .addCase(changeFullName.pending, (state) => {
            state.isLoading = true;
            state.error = undefined;
            state.authenticated = false;
          })
          .addCase(changeFullName.fulfilled, (state, action) => {
            state.isLoading = false;
            state.accessToken = action.payload.accessToken;
            state.refreshToken = action.payload.refreshToken;
            state.authenticated = true;
            console.log('Đổi tên thành công');
          })
          .addCase(changeFullName.rejected, (state, action: any) => {
            state.isLoading = false;
            state.error = action.payload?.error;
            state.authenticated = false;
            console.log('Đổi tên thất bại');
          });
      },
})

export default changeFullNameSlice.reducer;


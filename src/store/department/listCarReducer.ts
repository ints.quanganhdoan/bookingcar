import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import driverAPI from "app/axios/api/driverAPI";
import axios from "axios";

interface VehicleType {
    id: number;
    type: string;
    license_plate: string;
    seats: number;
    status_vehicle: string;
}

interface CarType {
    id: number;
    username: string;
    fullname: string;
    avatar: string;
    vehicle: VehicleType; 
}

export interface ListCarState {
    isLoading: boolean;
    error: string | undefined;
    cars: CarType[];
}

const initialState: ListCarState = {
    isLoading: false,
    error: undefined,
    cars: []
}

interface AccessTokenPayload {
    accessToken: string;
}

export const getListCar = createAsyncThunk(
    'fetch/listCar',
    async (payload: AccessTokenPayload, thunkAPI) => {
        const { accessToken } = payload;
        try {
            const response = await axios.get(driverAPI.listDriverApi, {
                headers: {
                    "Authorization": `Bearer ${accessToken}`
                }
            });
            return response.data;
        } catch (err: any) {
            return thunkAPI.rejectWithValue(err.message);
        }
    }
);

const listCarSlice = createSlice({
    name: 'listCar',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getListCar.pending, (state) => {
                state.isLoading = true;
                state.error = undefined;
            })
            .addCase(getListCar.fulfilled, (state, action) => { 
                state.isLoading = false;
                state.error = undefined;
                state.cars = action.payload; 
            })
            .addCase(getListCar.rejected, (state, action) => { 
                state.isLoading = false;
                state.error = action.error.message;
            });
    }
});

export default listCarSlice.reducer;

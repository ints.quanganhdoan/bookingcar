import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import driverAPI from "app/axios/api/driverAPI";
import axios from "axios";

interface OrderCarPayLoadType {
    date_start: string;
    time_start: string;
    date_end: string;
    time_end: string;
    location_start: string;
    location_end: string;
    numbers_passenger: number;
    position_title: string;
    fullname_booking: string;
    reason_booking: string;
    area: string;
}


export interface ListCarState {
    isLoading: boolean;
    error: string | undefined;
}

const initialState: ListCarState = {
    isLoading: false,
    error: undefined,
}

interface AccessTokenPayload {
    accessToken: string;
}

export const orderCar = createAsyncThunk(
    'orderCar',
    async (payload: {accessToken: AccessTokenPayload; id: string; orderPayload: OrderCarPayLoadType}, thunkAPI) => {
        const { accessToken, id, orderPayload  } = payload;
        try {
            const response = await axios.post(`${driverAPI.orderDriverApi}?id_driver=${id}`, orderPayload, {
                headers: {
                    "Authorization": `Bearer ${accessToken}`
                }
            });
            return response.data;
        } catch (err: any) {
            console.log(err);
            return thunkAPI.rejectWithValue(err.message);
        }
    }
);

const orderCarSlice = createSlice({
    name: 'orderCar',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(orderCar.pending, (state) => {
                state.isLoading = true;
                state.error = undefined;
            })
            .addCase(orderCar.fulfilled, (state, action) => { 
                state.isLoading = false;
                state.error = undefined;
                console.log('order car thành công!');
            })
            .addCase(orderCar.rejected, (state, action) => { 
                state.isLoading = false;
                state.error = action.error.message;
                console.log('order car không thành công!');
                console.log(action.error.message);
            });
    }
});

export default orderCarSlice.reducer;

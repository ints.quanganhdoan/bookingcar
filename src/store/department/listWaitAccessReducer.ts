import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import driverAPI from "app/axios/api/driverAPI";
import axios from "axios";

interface driverType {
    avatar: string;
    fullname: string;
}


export interface listWaitAccessType {
    id_booking: number;
    date_start: string;
    date_end: string;
    time_start: string;
    time_end: string;
    reason_booking: string;
    status_booking: string;
    driver: driverType;
}

interface listWaitAccessState {
    listWaitAcess: listWaitAccessType[];
    isLoading: boolean;
    error: string | undefined;
}

export const fetchListWaitAccess = createAsyncThunk(
    'fetch/listWaitAccess',
    async(accessToken: string,thunkAPI) => {
        try {
            const response = await axios.get(driverAPI.listWaitAccessDriverApi, {
                headers: {
                    "Authorization": `Bearer ${accessToken}`
                }
            });
            return response.data;
        } catch (err: any) {
            return thunkAPI.rejectWithValue(err.message);
        }
    }
)

const initialState: listWaitAccessState = {
    listWaitAcess: [],
    isLoading: false,
    error: undefined,
}

const listWaitAccessSlice = createSlice({
    name: 'listWaitAccess',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchListWaitAccess.pending, (state) => {
                state.isLoading = true;
                state.error = undefined;
            })
            .addCase(fetchListWaitAccess.fulfilled, (state, action) => {
                state.isLoading = false;
                state.error = undefined;
                state.listWaitAcess = action.payload;
            })
            .addCase(fetchListWaitAccess.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.error.message;
            })
    }
})

export default listWaitAccessSlice.reducer;


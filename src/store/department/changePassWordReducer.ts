import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import changeUserInforApi from 'app/axios/api/changeUserInforApi';

interface UserInforState {
    isLoading: boolean;
    error: string | undefined;
    refreshToken: string | undefined;
    accessToken: string | undefined;
    authenticated: boolean;
}

interface PassWordType {
    passwordOld: string;
    passwordNew: string;
    repeatPassword: string;
}


const initialState: UserInforState = {
    isLoading: false,
    error: undefined,
    accessToken: undefined,
    refreshToken: undefined,
    authenticated: false,
}


export const changePassword = createAsyncThunk(
    'change/password',
    async(payload: {accessToken: string; password: PassWordType}, thunkAPI) => {
      const { accessToken, password } = payload;
        try{
            const response = await axios.patch(changeUserInforApi.changePassword, password, {
              headers: {
                "Authorization": `Bearer ${accessToken}`
              }
            });
            return response.data;
        } catch (error: any){
            return thunkAPI.rejectWithValue({ error: error.message });
        }
    }
) 

const changePasswordSlice = createSlice({
    name: 'changePassWord',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
          .addCase(changePassword.pending, (state) => {
            state.isLoading = true;
            state.error = undefined;
            state.authenticated = false;
          })
          .addCase(changePassword.fulfilled, (state, action) => {
            state.isLoading = false;
            state.accessToken = action.payload.accessToken;
            state.refreshToken = action.payload.refreshToken;
            state.authenticated = true;
            console.log('Đổi mật khẩu thành công');
          })
          .addCase(changePassword.rejected, (state, action: any) => {
            state.isLoading = false;
            state.error = action.payload?.error;
            state.authenticated = false;
            console.log('Đổi mật khẩu thất bại');
          });
      },
})

export default changePasswordSlice.reducer;


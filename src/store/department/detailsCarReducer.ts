import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import driverAPI from "app/axios/api/driverAPI";
import axios from "axios";

interface VehicleType {
    id_vehicle: number;
    type: string;
    license_plate: string;
    seats: number;
    status_vehicle: string;
}

interface CarType {
    id_user: number;
    username: string;
    fullname: string;
    avatar: string;
    vehicle: VehicleType; 
}

export interface ListCarState {
    isLoading: boolean;
    error: string | undefined;
    car: CarType;
}

const initialState: ListCarState = {
    isLoading: false,
    error: undefined,
    car: {
        id_user: 0,
        username: "",
        fullname: "",
        avatar: "",
        vehicle: {
            id_vehicle: 0,
            type: "",
            license_plate: "",
            seats: 0,
            status_vehicle: ""
        }
    }
}

interface AccessTokenPayload {
    accessToken: string;
}


export const getDetailsCar = createAsyncThunk(
    'fetch/detailsCar',
    async (payload: {accessToken: AccessTokenPayload; id: string}, thunkAPI) => {
        const { accessToken, id } = payload;
        try {
            const response = await axios.get(`${driverAPI.detailsDriverApi}?id=${id}`, {
                headers: {
                    "Authorization": `Bearer ${accessToken}`
                }
            });
            return response.data;
        } catch (err: any) {
            return thunkAPI.rejectWithValue(err.message);
        }
    }
);

const detailsCarSlice = createSlice({
    name: 'detailsCar',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getDetailsCar.pending, (state) => {
                state.isLoading = true;
                state.error = undefined;
            })
            .addCase(getDetailsCar.fulfilled, (state, action) => {
                state.isLoading = false;
                state.error = undefined;
                state.car = action.payload;
            })
            .addCase(getDetailsCar.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.error.message;
            })
    }
})

export default detailsCarSlice.reducer;
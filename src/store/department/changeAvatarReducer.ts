import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import changeUserInforApi from "app/axios/api/changeUserInforApi";
import axios from "axios";

interface UserInforState {
    isLoading: boolean;
    error: string | undefined;
    refreshToken: string | undefined;
    accessToken: string | undefined;
    authenticated: boolean;
}

const initialState: UserInforState = {
    isLoading: false,
    error: undefined,
    accessToken: undefined,
    refreshToken: undefined,
    authenticated: false,
}

export const changeAvatar = createAsyncThunk(
    'change/avatar',
    async(payload: {accessToken: string; formData: FormData}, thunkAPI) => {
      const { accessToken, formData } = payload;
        try{
            const response = await axios.patch(changeUserInforApi.updateAvatar, formData, {
              headers: {
                "Authorization": `Bearer ${accessToken}`
              }
            });
            return response.data;
        } catch (error: any){
            return thunkAPI.rejectWithValue({ error: error.message });
        }
    }
)

const changeAvatarSlice = createSlice({
    name: 'changeAvatar',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
          .addCase(changeAvatar.pending, (state) => {
            state.isLoading = true;
            state.error = undefined;
            state.authenticated = false;
          })
          .addCase(changeAvatar.fulfilled, (state, action) => {
            state.isLoading = false;
            state.accessToken = action.payload.accessToken;
            state.refreshToken = action.payload.refreshToken;
            state.authenticated = true;
            console.log('Đổi ảnh thành công');
          })
          .addCase(changeAvatar.rejected, (state, action: any) => {
            state.isLoading = false;
            state.error = action.payload?.error;
            state.authenticated = false;
            console.log('Đổi ảnh thất bại');
          });
      },
})

export default changeAvatarSlice.reducer;
 
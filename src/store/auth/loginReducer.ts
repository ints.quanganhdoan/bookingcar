import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import authApi from 'app/axios/api/authApi';

interface User {
    id_user: number;
    id_employee: string;
    username: string;
    fullname: string;
    avatar: string | null;
    role: string;
    department: {
      id_department: number;
      name_department: string;
      description: string;
    };
    vehicle: {
      id_vehicle: number;
      type: string;
      license_plate: string;
      seats: number;
      status_vehicle: string;
    };
  }
  
interface LoginState {
      isLoading: boolean;
      error: string | undefined;
      refreshToken: string | undefined;
      accessToken: string | undefined;
      authenticated: boolean;
      user: User | undefined; 
  }

export interface LoginPayload {
    username: string;
    password: string;
}

const initialState: LoginState = {
    isLoading: false,
    error: undefined,
    accessToken: undefined,
    refreshToken: undefined,
    authenticated: false,
    user: undefined,
}

export const login = createAsyncThunk(
    'auth/login',
    async (payload: LoginPayload, thunkAPI) => {
      try {
        const response = await axios.post(authApi.loginApi, payload);
        return response.data;
      } catch (error: any) {
        return thunkAPI.rejectWithValue({ error: error.message });
      }
    }
);

const loginSlice = createSlice({
    name: 'login',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
      builder
        .addCase(login.pending, (state) => {
          state.isLoading = true;
          state.error = undefined;
          state.authenticated = false;
          state.user = undefined;
        })
        .addCase(login.fulfilled, (state, action) => {
          state.isLoading = false;
          state.accessToken = action.payload.accessToken;
          state.refreshToken = action.payload.refreshToken;
          state.authenticated = true;
          state.user = action.payload.user;
          console.log('Đăng nhập thành công');
        })
        .addCase(login.rejected, (state, action: any) => {
          state.isLoading = false;
          state.error = action.payload?.error;
          state.authenticated = false;
          state.user = undefined;
          console.log('Đăng nhập thất bại');

        });
    },
  });

export default loginSlice.reducer;

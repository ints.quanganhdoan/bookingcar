import React, { useCallback } from 'react';

const useImageErrorHandling = (fallbackSrc: string) => {
  const handleError = useCallback((e: React.SyntheticEvent) => {
    const target = e.target as HTMLImageElement;
    target.onerror = null;
    target.src= fallbackSrc;
  }, [fallbackSrc]);

  return handleError;
};

export default useImageErrorHandling;

export const Epath = {
  // NotFoundPage
  notFoundPage: '/notfound',

  // LoginPage
  loginPage: '/login',

  // RegisterPage
  registerPage: '/register',

  // Home Page
  homePage: '/',

  // Department Home Page
  departmentHomePage: '/department',
  departmentDriverPage: '/department/drivers',
  departmentDetailsDriverPage: '/department/drivers/:id',
  departmentOrderDriverDetailsPage: '/department/drivers/:id/order_details',
  departmentOrderDriverDetailsSuccess: '/department/drivers/:id/success',
  departmentOrderDriverDetailsFail: '/department/drivers/:id/fail',
  departmentUserInformation: '/department/information',
  departmentUserChangeInformation: '/department/information/edit',
  departmentUserChangePassword: '/department/information/changePassword',
  departmentOrderDriverHistory: '/department/order_history',
  departmentTripHistory: '/department/trip_history',

  // Driver Home Page
  driverHomePage: '/driver',

  // HR Home Page
  hrHomePage: '/manager',

  // BOD Home Page
  bodHomePage: '/bod', 

  // Not Allowed Page
  notAllowedPage: '/notAllowed'
};

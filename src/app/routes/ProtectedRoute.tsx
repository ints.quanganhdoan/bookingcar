import React from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';
import { getUserFromLocalStorage } from 'app/helpers/localStorage';
import { Epath } from 'app/routes/routesConfig';

interface ProtectedRouteProps extends RouteProps {
    allowedRole?: string;
    component: React.ComponentType<any>;
}

const ProtectedRoute: React.FC<ProtectedRouteProps> = ({allowedRole, component: Component, ...rest}) => {
    const userData: any = getUserFromLocalStorage();

    return (
        <Route {...rest} render={(props) => (
            allowedRole ? (userData.role === allowedRole
                ? <Component {...props} />
                : <Redirect to={Epath.homePage} />)
                : <Component {...props} />
        )} />
    );
};

export default ProtectedRoute;

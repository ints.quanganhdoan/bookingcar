import React, { Suspense, Fragment, lazy } from 'react';
import { Switch, Redirect, Route, useRouteMatch } from 'react-router-dom';
import { IndexedObject } from 'types/common';
import SuspenseFallback from '../components/Common/SuspenseFallback/SuspenseFallback';
import Layout, { PropsLayout } from '../components/Layout/Layout';
import { Epath } from './routesConfig';
import { getUserFromLocalStorage } from 'app/helpers/localStorage';
import ProtectedRoute from './ProtectedRoute';

export type RoutesProps = {
  exact?: boolean;
  path: string;
  component: React.FC<{ history: IndexedObject; location: IndexedObject; match: IndexedObject }>;
  auth?: boolean;
  routes?: Array<RoutesProps>;
  layout?: React.FC<PropsLayout>;
  allowedRole?: string;
};

const RenderRoutes = ({
  routes,
  isAuthenticated,
}: {
  routes: Array<RoutesProps>;
  isAuthenticated: boolean;
}) => {

  const match = useRouteMatch();

  const userData: any = getUserFromLocalStorage();
  
  return (
    <Suspense fallback={<SuspenseFallback />}>
      <Switch>
        {routes.map((route, i) => {
          const LayoutRoute = route.layout || Fragment;
          const Component = route.component || <div />;
          if (route.auth && !isAuthenticated) {
            return <Redirect key={i} to={Epath.loginPage} />;
          }
          return (
            <ProtectedRoute
              key={i}
              path={route.path}
              exact={!!route.exact}
              allowedRole={route.allowedRole}
              component={(props) => (
                <LayoutRoute>
                  {route.routes ? (
                    <RenderRoutes routes={route.routes} isAuthenticated={isAuthenticated} />
                  ) : (
                    <Component {...props} />
                  )}
                </LayoutRoute>
              )}
            />

            // <Route
            // key={i}
            // path={route.path}
            // exact={!!route.exact}
            // component={(props: any) => (
            //   <LayoutRoute>
            //     {route.routes ? (
            //       <RenderRoutes routes={route.routes} isAuthenticated={isAuthenticated} />
            //     ) : (
            //       <Component {...props} />
            //     )}
            //   </LayoutRoute>
            // )}
            // />

          );
        })}
      </Switch>
    </Suspense>
  );
};

export const routes = [
  {
    exact: true,
    path: Epath.notFoundPage,
    component: lazy(() => import('../pages/NotFoundPage/NotFoundPage')),
  },
  {
    exact: true,
    path: Epath.loginPage,
    component: lazy(() => import('../pages/AuthPage/LoginPage/LoginPage')),
  },
  {
    exact: true,
    path: Epath.registerPage,
    component: lazy(() => import('../pages/AuthPage/RegisterPage/RegisterPage'))
  },
  {
    exact: true,
    path: Epath.notAllowedPage,
    component: lazy(() => import('../pages/NotAllowedPage/NotAllowedPage')),
  },
  {
    path: '*',
    layout: Layout,
    component: () => <Redirect to={Epath.homePage} />,
    routes: [
      {
        exact: true,
        path: Epath.homePage,
        component: lazy(() => import('../pages/HomePage/HomePage')),
        auth: true
      },
      {
        exact: true,
        path: Epath.departmentHomePage,
        component: lazy(() => import('../pages/HomePage/DepartmentHomePage/DepartmentHomePage')),
        auth: true,
        allowedRole: 'PHONGBAN',
      },
      {
        exact: true,
        path: Epath.departmentDriverPage,
        component: lazy(() => import('../pages/DepartmentPages/OderDriverPage/OrderDriverPage')),
        auth: true,
        allowedRole: 'PHONGBAN',
      },
      {
        exact: true,
        path: Epath.departmentDetailsDriverPage,
        component: lazy(() => import('../pages/DepartmentPages/DetailsDriverPage/DetailsDriverPage')),
        auth: true,
        allowedRole: 'PHONGBAN',
      },
      {
        exact: true,
        path: Epath.departmentOrderDriverDetailsPage,
        component: lazy(() => import('../pages/DepartmentPages/OrderDriverDetailsPage/OrderDriverDetailsPage')),
        auth: true,
        allowedRole: 'PHONGBAN',
      },
      {
        exact: true,
        path: Epath.departmentOrderDriverDetailsSuccess,
        component: lazy(() => import('../pages/DepartmentPages/OrderDriverDetailsPage/OrderDriverSuccess/OrderDriverSuccess')),
        auth: true,
        allowedRole: 'PHONGBAN',
      },
      {
        exact: true,
        path: Epath.departmentOrderDriverDetailsFail,
        component: lazy(() => import('../pages/DepartmentPages/OrderDriverDetailsPage/OrderDriverFail/OrderDriverFail')),
        auth: true,
        allowedRole: 'PHONGBAN',
      },
      {
        exact: true,
        path: Epath.departmentUserInformation,
        component: lazy(() => import('../pages/CommonPage/UserInformation/UserInformation')),
        auth: true,
        allowedRole: 'PHONGBAN',
      },
      {
        exact: true,
        path: Epath.departmentUserChangeInformation,
        component: lazy(() => import('../pages/CommonPage/ChangeUserInfor/ChangeUserInfor')),
        auth: true,
        allowedRole: 'PHONGBAN',
      },
      {
        exact: true,
        path: Epath.departmentUserChangePassword,
        component: lazy(() => import('../pages/CommonPage/ChangePassWord/ChangePassWord')),
        auth: true,
        allowedRole: 'PHONGBAN',
      },
      {
        exact: true,
        path: Epath.departmentOrderDriverHistory,
        component: lazy(() => import('../pages/DepartmentPages/OrderDriverHistory/OrderDriverHistory')),
        auth: true,
        allowedRole: 'PHONGBAN',
      },
      {
        exact: true,
        path: Epath.departmentTripHistory,
        component: lazy(() => import('../pages/DepartmentPages/TripHistory/TripHistory')),
        auth: true,
        allowedRole: 'PHONGBAN',
      },
      {
        exact: true,
        path: Epath.driverHomePage,
        component: lazy(() => import('../pages/HomePage/DriverHomePage')),
        auth: true,
        allowedRole: 'TAIXE'
      },
      {
        exact: true,
        path: Epath.hrHomePage,
        component: lazy(() => import('../pages/HomePage/HRHomePage')),
        auth: true,
        allowedRole: 'HANHCHINH'
      },
      {
        exact: true,
        path: Epath.bodHomePage,
        component: lazy(() => import('../pages/HomePage/BODHomePage')),
        auth: true,
        allowedRole: 'BANGIAMDOC'
      },
      {
        exact: true,
        path: '*',
        component: () => <Redirect to={Epath.notFoundPage} />,
      },
    ],
  },
];

export default RenderRoutes;

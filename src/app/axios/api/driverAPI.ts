const driverAPI = {
    listDriverApi: 'https://trankhai-srsquanlyxe.up.railway.app/api/v2/users/listUser',
    detailsDriverApi: 'https://trankhai-srsquanlyxe.up.railway.app/api/v2/users',
    orderDriverApi: 'https://trankhai-srsquanlyxe.up.railway.app/api/v2/booking-details/create',
    listWaitAccessDriverApi: 'https://trankhai-srsquanlyxe.up.railway.app/api/v2/booking-details/listWaitAccess'
}

export default driverAPI;
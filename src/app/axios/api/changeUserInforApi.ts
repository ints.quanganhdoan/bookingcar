const changeUserInforApi = {
    updateName: 'https://trankhai-srsquanlyxe.up.railway.app/api/v2/users/updateInfor',
    updateAvatar: 'https://trankhai-srsquanlyxe.up.railway.app/api/v2/users/uploadAvatar',
    changePassword: 'https://trankhai-srsquanlyxe.up.railway.app/api/v2/users/updatePassword'
}

export default changeUserInforApi;
import UserInfor from 'app/components/Common/User/UserInfor';
import { getTokenFromLocalStorage, getUserFromLocalStorage } from 'app/helpers/localStorage';
import React, { useEffect } from 'react';
import styles from './style.module.scss';
import Menu from 'app/components/Common/Menu/Menu';
import { useHistory } from 'react-router-dom';
import {FaHistory, FaMapMarkerAlt} from 'react-icons/fa'
import ListCar, { ListCarType } from 'app/components/Common/ListCar/ListCar';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types/RootState';
import { getListCar } from 'store/department/listCarReducer';
import { Epath } from 'app/routes/routesConfig';
import { useMenuDepartmentData } from 'app/data/MenuData';

type Props = {};

const DepartmentHomePage = (props: Props) => {

  const userData: any = getUserFromLocalStorage();
  const accessToken: any = getTokenFromLocalStorage();
  
  const history = useHistory();
  
  const dispatch = useDispatch();

  const listDriver = useSelector((state: RootState) => state.listDriver.cars);
  const readyDriver = listDriver && listDriver.length > 0 ? listDriver.filter((car: ListCarType) => car.vehicle.status_vehicle === 'Sẵn sàng') : [];

  useEffect(() => {
    dispatch(getListCar({ accessToken: accessToken }));
  }, []);

  const {carClick, inforClick, historyOrderCar, historyTrip} = useMenuDepartmentData();

  return (
    <div className={styles.container}>
      <UserInfor 
        departmentRole={userData.role} 
        fullname={userData.fullname} 
        avatar={userData.avatar} 
        className={styles.userInfor}/>
      <h1 className={styles.title}>Hướng dẫn sử dụng app đặt xe</h1>
      <ul>
        <li>Sử dụng thông tin tài khoản được cấp từ Admin hệ thống để đăng nhập vào hệ thống đặt xe của công ty</li>
        <li>Chọn loại xe, tuyến đường, và thời gian bạn muốn sử dụng. Xác nhận yêu cầu đặt xe của bạn.</li>
        <li>Nếu cần, bạn có thể hủy đặt xe trước thời gian quy định. Đảm bảo xác nhận việc hủy đặt xe để tránh phí phạt</li>
        <li>Theo dõi thông tin về đặt xe, quá trình xét duyệt, cũng như hành trình di chuyển của xe.</li>
        <li>Nếu có thay đổi về số điện thoại, hãy liên hệ với Admin hoặc người quản lý để cập nhật thông tin liên hệ của bạn.</li>
      </ul>
      <h1 className={styles.title}>Gợi ý đặt xe</h1>
      <ListCar list={readyDriver} numberCars={10}/>
      <Menu homeActive={true} carClick={carClick} historyOrderCar={historyOrderCar} historyTrip={historyTrip} inforClick={inforClick}/>
    </div>
  )
};

export default DepartmentHomePage;

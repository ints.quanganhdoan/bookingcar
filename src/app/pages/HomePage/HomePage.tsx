import { getUserFromLocalStorage } from 'app/helpers/localStorage';
import { Epath } from 'app/routes/routesConfig';
import React from 'react';
import { Redirect } from 'react-router-dom';

const HomePage = () => {

    const userData: any = getUserFromLocalStorage();
    if (!userData || !userData.role) {
        return <Redirect to="/login" />; 
    }

    switch (userData.role) {
        case 'PHONGBAN':
            return <Redirect to={Epath.departmentHomePage} />;
        case 'TAIXE':
            return <Redirect to={Epath.driverHomePage} />;
        case 'HANHCHINH':
            return <Redirect to={Epath.hrHomePage} />;
        case 'BANGIAMDOC':
            return <Redirect to={Epath.bodHomePage} />;
        default:
            return <Redirect to={Epath.loginPage} />; 
    }
};

export default HomePage;
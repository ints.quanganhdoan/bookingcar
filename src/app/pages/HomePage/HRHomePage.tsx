import { getUserFromLocalStorage } from 'app/helpers/localStorage';
import { Epath } from 'app/routes/routesConfig';
import React, { useEffect } from 'react';
import { Redirect, useHistory } from 'react-router-dom';

const HumanResourceHomePage = () => {

    const userData: any = getUserFromLocalStorage();

    return (
        <div>
            HumanResource
        </div>
    );
};

export default HumanResourceHomePage;
import styles from './style.module.scss';
import { useDispatch } from "react-redux";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { AiFillEye, AiFillEyeInvisible }  from 'react-icons/ai';
import { useHistory } from "react-router-dom";
import { passwordRegex, userNameRegex } from "app/helpers/common";
import { Epath } from "app/routes/routesConfig";
import Header from 'app/components/Layout/Header/Header';
import Input from 'app/components/Common/Input/Input';


const RegisterPage: React.FC = () => {

    const history = useHistory();

    const {
        register,
        formState: { errors },
        handleSubmit,
    } = useForm();
    
    const[showPassword, setShowPassword] = useState<boolean>(false);

    const[showRePassword, setShowRePassword] = useState<boolean>(false);

    const[checkRePassword, setCheckRePassword] = useState<boolean>(true);

    const dispatch = useDispatch();
    
    const onSubmit = (data: any) => {
        if(data.password!==data.rePassword){
          setCheckRePassword(false);
        }
        console.log(data);
    }

    return(
        <>
      <Header />
      <div className={styles.registerContainer}>
        <div>
          <span className={styles.registerLogoFirstChar}>Booking</span>
          <span className={styles.registerLogoSecondChar}>Car</span>
        </div>
        <h1 className={styles.registerTitle}>Signup</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            {...register("username", { 
              required: true,
              pattern: userNameRegex })}
            label='Username'
            aria-invalid={errors.username ? "true" : "false"}
            placeholder='Phone number'
          />
          {errors.username?.type === "pattern" && (
            <p role="alert" className={styles.registerError}>Tên đăng nhập phải là số điện thoại</p>
          )}
          <select {...register("departmentName", { required: true })} className={styles.registerSelect}>
              <option value="">Chọn phòng ban</option>
              <option value="PHONGBAN">Phòng ban</option>
              <option value="TAIXE">Tài xế</option>
              <option value="HANHCHINH">Hành chính</option>
              <option value="BANGIAMDOC">Ban giám đốc</option>
          </select>
          <div className={styles.registerPasswordField}>
            <Input
              {...register("password", 
              { required: "Password is required",
                pattern: passwordRegex })}
              label='Password'
              aria-invalid={errors.password ? "true" : "false"}
              type={showPassword ? 'text' : 'password'}
              placeholder='Password'
              maxLength={6}
            />
            {showPassword
            ?
            <AiFillEye className={styles.registerPasswordIcon} 
            onClick={() => setShowPassword(!showPassword)}/>
            :
            <AiFillEyeInvisible className={styles.registerPasswordIcon}
            onClick={() => setShowPassword(!showPassword)}/> 
            }
          </div>
          {errors.password?.type === "pattern" && (<p role="alert" className={styles.registerError}>
            Mật khẩu phải là một chuỗi số và có chứa đúng 6 kí tự
          </p>)}
          <div className={styles.registerPasswordField}>
            <Input
              {...register("rePassword", 
              { required: "Password is required",
                pattern: passwordRegex })}
              label='Re-password'
              aria-invalid={errors.rePassword ? "true" : "false"}
              type={showRePassword ? 'text' : 'password'}
              placeholder='Re-Password'
              maxLength={6}
            />
            {showRePassword
            ?
            <AiFillEyeInvisible className={styles.registerPasswordIcon} 
            onClick={() => setShowRePassword(!showRePassword)}/>
            :
            <AiFillEye className={styles.registerPasswordIcon}
            onClick={() => setShowRePassword(!showRePassword)}/> 
            }
          </div>
          {checkRePassword===false && (<p role="alert" className={styles.registerError}>
            Mật khẩu nhập phải giống nhau
          </p>)}

          <input 
          type="submit" 
          value="Signup"
          className={styles.registerBtn}/>
        </form>
        <p className={styles.registerSignup}>
          or 
          <span 
          className={styles.registerSignup_special}
          onClick={() => history.push(Epath.loginPage)}>
            Login
          </span>
        </p>
        <img src="/images/Pages/Auth/background.png" alt="background" className={styles.registerBackground}/>
        </div>
        </>
    )
}

export default RegisterPage;
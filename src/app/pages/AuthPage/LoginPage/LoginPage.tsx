import React, { useState } from 'react';
import styles from './style.module.scss';
import { useForm } from 'react-hook-form';
import { AiFillEye, AiFillEyeInvisible }  from 'react-icons/ai';
import { useDispatch } from 'react-redux';
import { login } from 'store/auth/loginReducer';
import { useHistory } from 'react-router-dom';
import { passwordRegex, userNameRegex } from 'app/helpers/common';
import { saveAuthenticated, saveRefreshToken, saveToken, saveUser } from 'app/helpers/localStorage';
import { Epath } from 'app/routes/routesConfig';
import Header from 'app/components/Layout/Header/Header';
import Input from 'app/components/Common/Input/Input';

const LoginPage: React.FC = () => {

const history = useHistory();

const {
    register,
    formState: { errors },
    handleSubmit,
} = useForm();

const[showPassword, setShowPassword] = useState<boolean>(false);

const dispatch = useDispatch();

const onSubmit = async(data: any) => {
  console.log(data);
  try{
    const response: any = await dispatch(login(data)); 
    saveToken(response.payload.accessToken);
    saveRefreshToken(response.payload.refreshToken);
    saveUser(response.payload.user);
    saveAuthenticated('true');
    console.log(response);
    history.push('/');
  } catch(error) {
    console.log(error);
  }
};

  return (
    <>
      <Header />
      <div className={styles.loginContainer}>
        <div>
          <span className={styles.loginLogoFirstChar}>Booking</span>
          <span className={styles.loginLogoSecondChar}>Car</span>
        </div>
        <h1 className={styles.loginTitle}>Login</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            {...register("username", { 
              required: true,
              pattern: userNameRegex })}
            label='Username'
            aria-invalid={errors.username ? "true" : "false"}
            placeholder='Phone number'
          />
          {errors.username?.type === "pattern" && (
            <p role="alert" className={styles.loginError}>Tên đăng nhập phải là số điện thoại</p>
          )}
          <div className={styles.loginPasswordField}>
            <Input
              {...register("password", 
              { required: "Password is required",
                pattern: passwordRegex })}
              label='Password'
              aria-invalid={errors.password ? "true" : "false"}
              type={showPassword ? 'text' : 'password'}
              placeholder='Password'
              maxLength={6}
            />
            {showPassword
            ?
            <AiFillEye className={styles.loginPasswordIcon} 
            onClick={() => setShowPassword(!showPassword)}/>
            :
            <AiFillEyeInvisible className={styles.loginPasswordIcon}
            onClick={() => setShowPassword(!showPassword)}/> 
            }
          </div>
          {errors.password?.type === "pattern" && (<p role="alert" className={styles.loginError}>
            Mật khẩu phải là một chuỗi số và có chứa đúng 6 kí tự
          </p>)}

          <input 
          type="submit" 
          value="Login"
          className={styles.loginBtn}/>
        </form>
        <p className={styles.loginSignup}>
          or 
          <span className={styles.loginSignup_special}
          onClick={() => {
            history.push(Epath.registerPage)
          }}>
            Signup
          </span>
        </p>
        <img src="/images/Pages/Auth/background.png" alt="background" className={styles.loginBackground}/>
      </div>
    </>
  );
};

export default LoginPage;

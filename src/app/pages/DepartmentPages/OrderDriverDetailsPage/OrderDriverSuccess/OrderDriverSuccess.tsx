import React from 'react';
import styles from './style.module.scss';
import { useHistory } from 'react-router-dom';
import { Epath } from 'app/routes/routesConfig';

const OrderDriverSuccess = () => {
    const histoy = useHistory();

    const handleHomepageClick = () => {
        histoy.push(Epath.homePage);
    }

    const handleDriverClick = () => {
        histoy.push(Epath.departmentDriverPage)
    }

    return (
        <div className={styles.container}>
            <img 
            src="/images/Pages/Department/success_order.png" 
            alt="success image"
            className={styles.img_success} 
            />
            <div className={styles.infor_container}>
                <p className={styles.title}>Yêu cầu đặt xe thành công</p>
                <p className={styles.subTitle}>Vui lòng chờ duyệt xe</p>
            </div>
            <button onClick={handleHomepageClick} className={styles.homepageBtn}>
                Về trang chủ
            </button>
            <button 
            onClick={handleDriverClick}
            className={styles.cancelBtn}>
                Đặt xe khác
            </button>
        </div>
    );
};

export default OrderDriverSuccess;
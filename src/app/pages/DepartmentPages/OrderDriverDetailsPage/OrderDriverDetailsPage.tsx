import React from 'react';
import styles from './style.module.scss';
import Title from 'app/components/Common/Title/Title';
import { Controller, useForm } from 'react-hook-form';
import { getTokenFromLocalStorage, getUserFromLocalStorage } from 'app/helpers/localStorage';
import Input from 'app/components/Common/Input/Input';
import InputTime from 'app/components/Common/InputTime/InputTime';
import InputDate from 'app/components/Common/InputDate/InputDate';
import { useHistory, useParams } from 'react-router-dom';
import Select from 'app/components/Common/Select/Select';
import { useDispatch } from 'react-redux';
import { orderCar } from 'store/department/orderCarReducer';

const OrderCarDetailsPage = () => {

    const { control, handleSubmit, formState: { errors } } = useForm();
    const history = useHistory();
    const userData: any = getUserFromLocalStorage();
    const accessToken: any = getTokenFromLocalStorage();
    const dispatch = useDispatch();
    const { id } = useParams<{ id: string }>();

    const onSubmit = (data: any) => {
        data.numbers_passenger = parseInt(data.numbers_passenger);
        console.log(data);
        dispatch(orderCar({
            accessToken: accessToken,
            id: id,
            orderPayload: data
        }));
    }
    return (
        <>
            <Title titleName={'Thông tin đặt xe'} />
            <form onSubmit={handleSubmit(onSubmit)} className={styles.form_container}>
                <Controller
                    name="fullname_booking"
                    control={control}
                    rules={{ required: 'Họ tên không được để trống' }}
                    defaultValue={''}
                    render={({ field }) =>
                    <>
                        <Input label="Họ tên" placeholder="Nhập họ tên" {...field} />
                        {errors.fullname_booking && <span className={styles.error_message}>{errors.fullname_booking.message}</span>}
                    </> 
                    }
                /> 
                <Input label={'Phòng ban'} placeholder={userData.role || 'Lỗi thông tin'} disabled={true} />
                <Controller
                    name="date_start"
                    control={control}
                    rules={{ required: 'Ngày đi dự kiến không được để trống' }}
                    defaultValue={''}
                    render={({ field }) =>
                    <>
                        <InputDate label={'Ngày đi dự kiến'} placeholder={'Chọn thời gian đi'} field={field}/>
                        {errors.date_start && <span className={styles.error_message}>{errors.date_start.message}</span>}
                    </> 
                    }
                />
                <Controller
                    name="time_start"
                    control={control}
                    rules={{ required: 'Thời gian đi không được để trống' }}
                    defaultValue={''}
                    render={({ field }) =>
                    <>
                        <InputTime label={'Thời gian đi dự kiến'} placeholder={'Chọn thời gian đi'} field={field}/>
                        {errors.time_start && <span className={styles.error_message}>{errors.time_start.message}</span>}
                    </> 
                    }
                />
                <Controller
                    name="date_end"
                    control={control}
                    rules={{ required: 'Ngày về không được để trống' }}
                    defaultValue={''}
                    render={({ field }) =>
                    <>
                        <InputDate label={'Ngày về dự kiến'} placeholder={'Chọn thời gian về'} field={field}/>
                        {errors.date_end && <span className={styles.error_message}>{errors.date_end.message}</span>}
                    </> 
                    }
                />
                <Controller
                    name="time_end"
                    control={control}
                    rules={{ required: 'Thời gian về không được để trống' }}
                    defaultValue={''}
                    render={({ field }) =>
                    <>
                        <InputTime label={'Thời gian về dự kiến'} placeholder={'Chọn thời gian về'} field={field}/>
                        {errors.time_end && <span className={styles.error_message}>{errors.time_end.message}</span>}
                    </> 
                    }
                />
                <Controller
                    name="location_start"
                    control={control}
                    rules={{ required: 'Địa điểm xuất phát không được để trống' }}
                    defaultValue={''}
                    render={({ field }) =>
                        <>
                            <Input label={'Địa điểm xuất phát'} placeholder={'Địa điểm đón'} {...field} />
                            {errors.location_start && <span className={styles.error_message}>{errors.location_start.message}</span>}
                        </> 
                    }
                />
                <Controller
                    name="location_end"
                    control={control}
                    rules={{ required: 'Địa điểm đến không được để trống' }}
                    defaultValue={''}
                    render={({ field }) =>
                    <>
                        <Input label={'Địa điểm đến'} placeholder={'Địa điểm muốn tới'} {...field} />
                        {errors.location_end && <span className={styles.error_message}>{errors.location_end.message}</span>}
                    </> 
                    }
                />
                <Controller
                    name="numbers_passenger"
                    control={control}
                    rules={{ required: 'Số người đi không được để trống' }}
                    defaultValue={''}
                    render={({ field }) => 
                    <>
                        <Input label={'Số người đi'} placeholder={'Nhập số người đi xe'} type='number' {...field} />
                        {errors.numbers_passenger && <span className={styles.error_message}>{errors.numbers_passenger.message}</span>}
                    </>
                    }
                />
                <Controller
                    name="position_title"
                    control={control}
                    rules={{ required: 'Chức danh người đi không được để trống' }}
                    defaultValue={''}
                    render={({ field }) => 
                    <>
                        <Input label={'Chức danh người đi'} placeholder={'Nhập chức danh người đi'} {...field} />
                        {errors.position_title && <span className={styles.error_message}>{errors.position_title.message}</span>}
                    </>
                    }
                />
                <Controller
                    name="reason_booking"
                    control={control}
                    rules={{ required: 'Lí do đặt xe không được để trống' }}
                    defaultValue={''}
                    render={({ field }) => 
                    <>
                        <Input label={'Lí do'} placeholder={'Nhập lí do đặt xe'} {...field} />
                        {errors.position_title && <span className={styles.error_message}>{errors.position_title.message}</span>}
                    </>
                    }
                />
                <Controller
                    name="area"
                    control={control}
                    defaultValue={'Nội Thành'}
                    render={({ field }) =>
                    <>
                        <Select 
                        options={[
                            {
                                label:'Nội Thành',
                                value:'Nội Thành'
                            },
                            {
                                label:'Ngoại Thành',
                                value:'Ngoại Thành'
                            },
                        ]} 
                        label={'Khu vực'} 
                        placeholder={'Chọn khu vực'} 
                        {...field} 
                        // onChange={(e) => field.onChange(e.target.value)}
                        />
                        {errors.area && <span className={styles.error_message}>{errors.area.message}</span>}
                    </> 
                    }
                />
                <div className={styles.btn_container}>
                    <input type="reset" value="Hủy bỏ" onClick={() => history.go(-1)} />
                    <input type="submit" value="Đặt xe" />
                </div>
            </form>
        </>
    );
};

export default OrderCarDetailsPage;
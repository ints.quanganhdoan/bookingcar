import React from 'react';
import styles from './style.module.scss';
import { useHistory } from 'react-router-dom';
import { Epath } from 'app/routes/routesConfig';

const OrderDriverSuccess = () => {
    const history = useHistory();

    const handleCancelClick = () => {
        history.push(Epath.departmentDetailsDriverPage)
    }

    const handleAgainClick = () => {
        history.go(-1);
    }


    return (
        <div className={styles.container}>
            <img 
            src="/images/Pages/Department/fail_order.jpg" 
            alt="success image"
            className={styles.img_fail} 
            />
            <div className={styles.infor_container}>
                <p className={styles.title}>Yêu cầu đặt xe thất bại</p>
                <p className={styles.subTitle}>Đã có lỗi xảy ra trong quá trình đặt xe của bạn</p>
            </div>
            <button onClick={handleAgainClick} className={styles.homepageBtn}>
                Thử lại
            </button>
            <button 
            onClick={handleCancelClick}
            className={styles.cancelBtn}>
                Hủy bỏ
            </button>
        </div>
    );
};

export default OrderDriverSuccess;
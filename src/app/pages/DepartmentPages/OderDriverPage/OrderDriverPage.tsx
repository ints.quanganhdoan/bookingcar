import ListCar, { ListCarType } from 'app/components/Common/ListCar/ListCar';
import Menu from 'app/components/Common/Menu/Menu';
import { getTokenFromLocalStorage } from 'app/helpers/localStorage';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { getListCar } from 'store/department/listCarReducer';
import { RootState } from 'types/RootState';
import styles from './style.module.scss';
import Title from 'app/components/Common/Title/Title';
import { useMenuDepartmentData } from 'app/data/MenuData';
import SearchField from 'app/components/Common/SearchField/SearchField';

const OrderCarPage = () => {

    const accessToken: any = getTokenFromLocalStorage();

    const history = useHistory();
    const[isReady, setIsReady] = useState<boolean>(true);

    const dispatch = useDispatch();

    const listDriver = useSelector((state: RootState) => state.listDriver.cars);

    const readyDriver = listDriver && listDriver.length > 0 ? listDriver.filter((car: ListCarType) => car.vehicle.status_vehicle === 'Sẵn sàng') : [];

    const unreadyDriver = listDriver && listDriver.length > 0 ? listDriver.filter((car: ListCarType) => car.vehicle.status_vehicle === 'Chưa sẵn sàng') : [];

    useEffect(() => {
      dispatch(getListCar({ accessToken: accessToken }));
    }, []);

    const {homeClick, inforClick, historyOrderCar, historyTrip} = useMenuDepartmentData();

    return (
        <div>
            <Title titleName={'Đặt xe'} />
            <div className={styles.button_container}>
                <button 
                className={isReady ? styles['active_btn'] : ''}
                onClick={() => setIsReady(true)}
                >
                    Sẵn sàng
                </button>
                <button 
                className={isReady ? '' : styles['active_btn']}
                onClick={() => setIsReady(false)}
                >
                    Đã được đặt
                </button>
            </div>
            <SearchField className={styles.searchField}/>
            <ListCar list={isReady ? readyDriver : unreadyDriver} numberCars={readyDriver.length} />
            <Menu carActive={true} homeClick={homeClick} historyOrderCar={historyOrderCar} historyTrip={historyTrip} inforClick={inforClick}/>
        </div>
    );
};

export default OrderCarPage;
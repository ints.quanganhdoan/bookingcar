import Title from 'app/components/Common/Title/Title';
import { getTokenFromLocalStorage } from 'app/helpers/localStorage';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { getDetailsCar } from 'store/department/detailsCarReducer';
import { RootState } from 'types/RootState';
import styles from './style.module.scss';
import Input from 'app/components/Common/Input/Input';
import useImageErrorHandling from 'app/helpers/customHook/useImageErrorHandling';
import { Epath } from 'app/routes/routesConfig';

const DetailsDriverPage = () => {

    const { id } = useParams<{ id: string }>();
    const history = useHistory();

    const dispatch = useDispatch();
    const detailsCar = useSelector((state: RootState) => state.detailsDriver.car);

    const accessToken: any = getTokenFromLocalStorage();

    const handleImageError = useImageErrorHandling('/images/Error/car_default.jpg');

    useEffect(() => {
        dispatch(getDetailsCar({accessToken: accessToken, id: id}))
    }, []);

    const handleOrderClick = () => {
        history.push(`${Epath.departmentDriverPage}/${id}/order_details`);
    }

    if(detailsCar===null){
        return <div>Loading ...</div>
    }

    return (
        <div>
            <Title titleName={'Chi tiết xe'} />
            <div className={styles.basicInfor}>
                <img 
                src={detailsCar.avatar || ''} 
                alt="image"
                onError={handleImageError} 
                />   
                <p className={detailsCar.vehicle? detailsCar.vehicle.status_vehicle ==='Sẵn sàng' ? styles['ready'] : '' : styles['busy']}>
                    Trạng thái: {detailsCar.vehicle? detailsCar.vehicle.status_vehicle : 'Loading...'}
                </p>
                <p>Xe {detailsCar.vehicle? detailsCar.vehicle.type : 'Loading ...'}</p>
            </div>
            <Input 
            label={'Biển số xe'} 
            placeholder={detailsCar.vehicle ? detailsCar.vehicle.license_plate : 'Loading ...'} 
            className={styles.inputItem}
            disabled={true}
            background={styles.inputBackground}
            />
            <Input 
            label={'Số lượng chỗ ngồi'} 
            placeholder={detailsCar.vehicle ? detailsCar.vehicle.seats : 'Loading ...'} 
            disabled={true}
            className={styles.inputItem}
            background={styles.inputBackground}
            />
            <Input 
            label={'Họ tên'} 
            placeholder={detailsCar.fullname} 
            disabled={true}
            className={styles.inputItem}
            background={styles.inputBackground}
            />
            <Input 
            label={'Số điện thoại'} 
            placeholder={detailsCar.username}
            disabled={true}
            className={styles.inputItem}
            background={styles.inputBackground}
            />
            <button 
            className={styles.btn}
            onClick={handleOrderClick}
            >
                Đặt xe
            </button>
        </div>
    );
};

export default DetailsDriverPage;
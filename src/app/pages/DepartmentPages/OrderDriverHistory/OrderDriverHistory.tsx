import Menu from 'app/components/Common/Menu/Menu';
import SearchField from 'app/components/Common/SearchField/SearchField';
import Title from 'app/components/Common/Title/Title';
import { useMenuDepartmentData } from 'app/data/MenuData';
import React, { useEffect, useState } from 'react';
import styles from './style.module.scss';
import OrderCarHistoryMenu from 'app/components/Common/OrderCarHistoryMenu/OrderCarHistoryMenu';
import ListCarHistory from 'app/components/Common/ListCarHistory/ListCarHistory';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types/RootState';
import { fetchListWaitAccess, listWaitAccessType } from 'store/department/listWaitAccessReducer';
import { getTokenFromLocalStorage } from 'app/helpers/localStorage';

const OrderCarHistory = () => {

    const {homeClick, carClick, inforClick, historyTrip} = useMenuDepartmentData();

    const accessToken: any = getTokenFromLocalStorage();
    
    const dispatch = useDispatch();
    const listWaitAccess = useSelector((state: RootState) => state.listWaitAccess.listWaitAcess);

    const waitingApproveList = listWaitAccess && listWaitAccess.length > 0 ? listWaitAccess.filter((waitItem: listWaitAccessType) => 
        waitItem.status_booking === 'Chờ duyệt') : [];

    const waitingCancelList = listWaitAccess && listWaitAccess.length > 0 ? listWaitAccess.filter((cancelItem: listWaitAccessType) => 
    cancelItem.status_booking === 'Chờ hủy') : [];

    const cancelList = listWaitAccess && listWaitAccess.length > 0 ? listWaitAccess.filter((cancelItem: listWaitAccessType) => 
    cancelItem.status_booking === 'Hủy') : [];

    const rejectList = listWaitAccess && listWaitAccess.length > 0 ? listWaitAccess.filter((cancelItem: listWaitAccessType) => 
    cancelItem.status_booking === 'Từ chối') : [];

    const listWaitAccessByStatus: { [key: string]: listWaitAccessType[] } = {
        'Chờ duyệt': waitingApproveList,
        'Chờ hủy': waitingCancelList,
        'Đã hủy': cancelList,
        'Bị từ chối': rejectList
    };

    const[status, setStatus] = useState<string>('Chờ duyệt');
    const currentList = listWaitAccessByStatus[status];

    useEffect(() => {
        dispatch(fetchListWaitAccess(accessToken));
    },[listWaitAccess])

    return (
        <>
            <div>
                <Title titleName={'Lịch sử đặt xe'} />
                <OrderCarHistoryMenu setStatus={setStatus}/>
                <SearchField className={styles.searchField}/>
                <ListCarHistory list={currentList} />
                <Menu 
                historyOrderCarActive={true}
                homeClick={homeClick} 
                carClick={carClick} 
                inforClick={inforClick} 
                historyTrip={historyTrip}/>
            </div>
        </>
    );
};

export default OrderCarHistory;
import Menu from 'app/components/Common/Menu/Menu';
import Title from 'app/components/Common/Title/Title';
import { useMenuDepartmentData } from 'app/data/MenuData';
import React, { useState } from 'react';
import styles from './style.module.scss';
import TripList from 'app/components/Common/TripList/TripList';

const TripHistory = () => {

    const {homeClick, carClick, inforClick, historyOrderCar} = useMenuDepartmentData();
    const[active, setActive] = useState<string>('upcoming');

    const[upcoming, setUpcoming] = useState<boolean>(true);

    const handleSetActive = (buttonActive: string) => {
        setActive(buttonActive);
    }

    return (
        <div>
            <Title titleName={'Lịch sử chuyến đi'} />
            <div className={styles.btn_container}>
                <button
                className={active === 'upcoming' ? styles.active : ''}
                onClick={() => {
                    handleSetActive('upcoming');
                    setUpcoming(!upcoming);
                }}
                >
                    Chuyến đi sắp tới
                </button>
                <button
                className={active === 'finish' ? styles.active : ''}
                onClick={() => {
                    handleSetActive('finish');
                    setUpcoming(!upcoming);
                }}
                >
                    Chuyến đã đi
                </button>
            </div>
            <TripList upcoming={upcoming}/>
            <TripList upcoming={upcoming}/>
            <TripList upcoming={upcoming}/>
            <Menu
            homeClick={homeClick}
            carClick={carClick}
            inforClick={inforClick}
            historyOrderCar={historyOrderCar} 
            historyTripActive={true}
            />
        </div>
    );
};

export default TripHistory;
import { Controller, useForm } from 'react-hook-form';
import Input from 'app/components/Common/Input/Input';
import Title from 'app/components/Common/Title/Title';
import { getTokenFromLocalStorage, getUserFromLocalStorage, saveUser } from 'app/helpers/localStorage';
import React, { useRef, useState } from 'react';
import styles from './style.module.scss';
import { useDispatch } from 'react-redux';
import { changeFullName } from 'store/department/changeNameReducer';
import { TbCameraPlus } from 'react-icons/tb';
import { changeAvatar } from 'store/department/changeAvatarReducer';
import Alert from 'app/components/Common/Modal/ModalAlert/Alert';
import useImageErrorHandling from 'app/helpers/customHook/useImageErrorHandling';

const ChangeUserInfor = () => {
    let userData: any = getUserFromLocalStorage();
    const accesToken = getTokenFromLocalStorage();

    const uploadRef = useRef<HTMLInputElement>(null);
    const[imgSrc, setImgSrc] = useState<string>(userData.avatar || '');
    const [file, setFile] = useState<File | null>(null);
    const[showSuccess, setShowSuccess] = useState<boolean>(false);
    const[showFail, setShowFail] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(false);


    const { control, handleSubmit } = useForm();

    const dispatch = useDispatch();

    const handleImageError = useImageErrorHandling('/images/Error/avatar_default.png');

    const handleUploadAvatar = () => {
        uploadRef.current?.click();
    };

    const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files?.[0];
        if (file) {
            setFile(file); 
            setImgSrc(URL.createObjectURL(file)); 
        } else {
            setImgSrc(userData.avatar);
        }
    }

    const onSubmit = async(data: any) => {
        console.log(data);
        if (accesToken !== null) {
            const formData = new FormData();
            if(file){
                formData.append('avatar', file);
            }
            setIsLoading(true);
            try {
                await dispatch(changeFullName({ accessToken: accesToken, fullname: data.fullname }));
                if(file) { 
                    await dispatch(changeAvatar({accessToken: accesToken, formData: formData}));
                    userData.avatar = imgSrc;
                }
                userData.fullname = data.fullname;
                userData.avatar = imgSrc;
                saveUser(userData);
                setIsLoading(false);
                setShowSuccess(true);
                setTimeout(() => {
                    setShowSuccess(false);
                }, 1500);
              } catch (error: any) {
                setIsLoading(false);
                setShowFail(true);
                setTimeout(() => {
                    setShowFail(false);
                }, 1500);
              }
        } else {
            setShowFail(true);
            setTimeout(() => {
                setShowFail(false);
            }, 1500);
            console.log("Access token is null");
        }
    }

    return(
        <>
            <Title titleName={'Thông tin cá nhân'} />
            <form onSubmit={handleSubmit(onSubmit)} className={styles.form_container}>
                <div className={styles.img_container}>
                    <img 
                    src={imgSrc} 
                    alt="avatar" 
                    onClick={handleUploadAvatar}
                    onError={handleImageError}
                    />
                    <div className={styles.avatar_icon_container} onClick={handleUploadAvatar}>
                        <TbCameraPlus className={styles.avatar_icon}/>
                    </div>
                    <input 
                    type="file" 
                    className={styles.avatar_upload} 
                    ref={uploadRef}
                    onChange={handleFileChange}/>
                </div>
                <Controller
                    name="fullname"
                    control={control}
                    defaultValue={userData.fullname}
                    render={({ field }) => <Input label={'Họ tên'} placeholder={userData.fullname||'Không tìm thấy thông tin'} className={styles.inputMargin} {...field} />}
                />
                <Controller
                    name="username"
                    control={control}
                    defaultValue={userData.username}
                    render={({ field }) => <Input label={'Số điện thoại'} placeholder={userData.username||'Không tìm thấy thông tin'} className={styles.inputMargin} {...{...field, disabled: true}} />}
                />
                <Controller
                    name="role"
                    control={control}
                    defaultValue={userData.role}
                    render={({ field }) => <Input label={'Thông tin phòng ban'} placeholder={userData.role||'Không tìm thấy thông tin'} className={styles.inputMargin} {...{...field, disabled: true}} />}
                />
                <Controller
                    name="id_user"
                    control={control}
                    defaultValue={userData.id_user}
                    render={({ field }) => <Input label={'Mã nhân viên'} placeholder={userData.id_user||'Không tìm thấy thông tin'} className={styles.inputMargin} {...{...field, disabled: true}} />}
                />
                <button type="submit" value="Lưu lại" className={styles.btn}>Submit</button>
            </form>
            {
                showSuccess ?
                <div className={styles.modal_overlay}>
                    <Alert contentModal={'Đổi thông tin thành công!'} typeModal={'Success'} />
                </div>
                :
                null
            }
            {
                showFail ?
                <div className={styles.modal_overlay}>
                    <Alert contentModal={'Đổi thông tin thất bại!'} typeModal={'Fail'} />
                </div>
                :
                null
            }
            {
                isLoading ?
                <div className={styles.modal_overlay}>
                <Alert contentModal={'Đang thay đổi thông tin...'} typeModal={'Loading'} />
                </div>
                :
                null
            }
        </>
    )
}

export default ChangeUserInfor;

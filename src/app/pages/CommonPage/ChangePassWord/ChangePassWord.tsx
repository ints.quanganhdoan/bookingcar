import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import styles from './style.module.scss';
import Title from 'app/components/Common/Title/Title';
import { getTokenFromLocalStorage } from 'app/helpers/localStorage';
import { useDispatch } from 'react-redux';
import { changePassword } from 'store/department/changePassWordReducer';
import Alert from 'app/components/Common/Modal/ModalAlert/Alert';
import PasswordField from 'app/components/Common/PasswordField/PasswordField';

const ChangePassWord = () => {

    const {
        register,
        reset,
        formState: { errors },
        handleSubmit,
    } = useForm();

    const accessToken = getTokenFromLocalStorage();

    const dispatch = useDispatch();

    const[showOldPassword, setShowOldPassword] = useState<boolean>(false);
    const[showNewPassword, setShowNewPassword] = useState<boolean>(false);
    const[showRepeatPassword, setShowRepeatPassword] = useState<boolean>(false);
    const[checkRePassword, setCheckRePassword] = useState<boolean>(true);

    const[showSuccess, setShowSuccess] = useState<boolean>(false);
    const[showFail, setShowFail] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const onSubmit = async (data: any) => {
        console.log(data);
        
        if(data.passwordNew!==data.repeatPassword){
            setCheckRePassword(false);
            return; 
        }

        if(accessToken!==null){

            setIsLoading(true);
            
            try{
                await dispatch(changePassword({accessToken: accessToken, password: data}));
                setIsLoading(false);
                setShowSuccess(true);
                setTimeout(() => {
                    setShowSuccess(false);
                }, 1500);
                reset();
            } catch (error: any){
                setIsLoading(false);
                setShowFail(true);
                setTimeout(() => {
                    setShowFail(false);
                }, 1500);
            }
        } else{
            setIsLoading(false);
            setShowFail(true);
            setTimeout(() => {
                setShowFail(false);
            }, 1500);
            console.log('Access Token is null!');  
        }
    }

    return (
        <>
            <Title titleName={'Đổi mật khẩu'}/>
            <form onSubmit={handleSubmit(onSubmit)} className={styles.form_container}>
                <PasswordField 
                name="passwordOld"
                label='Old Password' 
                placeholder='Old Password' 
                showPassword={showOldPassword} 
                setShowPassword={setShowOldPassword}
                register={register}
                errors={errors}
                />
                {errors.passwordOld?.type === "pattern" && (<p role="alert" className={styles.passwordError}>
                    Mật khẩu phải là một chuỗi số và có chứa đúng 6 kí tự
                </p>)}
                <PasswordField 
                name="passwordNew" 
                label='New Password' 
                placeholder='New Password' 
                showPassword={showNewPassword} 
                setShowPassword={setShowNewPassword}
                register={register}
                errors={errors}
                />
                {errors.passwordNew?.type === "pattern" && (<p role="alert" className={styles.passwordError}>
                    Mật khẩu phải là một chuỗi số và có chứa đúng 6 kí tự
                </p>)}
                <PasswordField 
                name="repeatPassword"
                label='Repeat Password' 
                placeholder='Repeat Password'  
                showPassword={showRepeatPassword} 
                setShowPassword={setShowRepeatPassword}
                register={register}
                errors={errors}
                />
                {checkRePassword===false && (<p role="alert" className={styles.passwordError}>
                    Mật khẩu nhập phải giống nhau
                </p>)}
                <input 
                type="submit" 
                value="Change Password"
                className={styles.passwordBtn}/>        
            </form>
            {
                isLoading ?
                <div className={styles.modal_overlay}>
                <Alert contentModal={'Đang thay đổi mật khẩu...'} typeModal={'Infor'} />
                </div>
                :
                null
            }
            {
                showSuccess ?
                <div className={styles.modal_overlay}>
                    <Alert contentModal={'Đổi mật khẩu thành công!'} typeModal={'Success'} />
                </div>
                :
                null
            }
            {
                showFail ?
                <div className={styles.modal_overlay}>
                    <Alert contentModal={'Đổi mật khẩu thất bại!'} typeModal={'Fail'} />
                </div>
                :
                null
            }
        </>
    );
};

export default ChangePassWord;
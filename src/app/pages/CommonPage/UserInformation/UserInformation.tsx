import Menu from 'app/components/Common/Menu/Menu';
import Title from 'app/components/Common/Title/Title';
import UserInfor from 'app/components/Common/User/UserInfor';
import { getUserFromLocalStorage, removeAuthenticated, removeRefreshToken, removeToken, removeUser } from 'app/helpers/localStorage';
import { Epath } from 'app/routes/routesConfig';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import styles from './style.module.scss';
import { DEFAULT_LANGUAGE, i18n } from 'locales/i18n';
import { useTranslation } from 'react-i18next';
import { BiLogOut } from "react-icons/bi";
import {BsFillPersonFill} from 'react-icons/bs';
import UserInforButton from 'app/components/Common/UserInforButton/UserInforButton';
import { FaHistory, FaMapMarkerAlt } from 'react-icons/fa';
import { RiLockPasswordFill } from 'react-icons/ri';
import Modal from 'app/components/Common/Modal/ModalOption/Modal';
import { useMenuDepartmentData } from 'app/data/MenuData';

const UserInformation = () => {

    const { t, i18n } = useTranslation();

    const userData: any = getUserFromLocalStorage();

    const history = useHistory();
    const[show, setShow] = useState(false);

    const {homeClick, carClick, historyOrderCar, historyTrip} = useMenuDepartmentData();

    const onLogOut = () => {
      i18n.changeLanguage(DEFAULT_LANGUAGE);
      removeToken();
      removeRefreshToken();
      removeUser();
      removeAuthenticated();
      history.push(Epath.loginPage);
    };

    return (
        <div>
            <Title titleName={'Thông tin'} />
            <UserInfor 
            username={userData.username}
            fullname={userData.fullname} 
            avatar={userData.avatar} 
            className={styles.userInfor}/>
            <UserInforButton 
            buttonName='Thông tin cá nhân' 
            onClick={() => history.push(Epath.departmentUserChangeInformation)} 
            children={<BsFillPersonFill />}/>
            <UserInforButton 
            buttonName='Lịch sử đặt xe' 
            onClick={() => history.push(Epath.departmentOrderDriverHistory)} 
            children={<FaMapMarkerAlt />}/>
            <UserInforButton 
            buttonName='Lịch sử chuyến đi' 
            onClick={() => history.push(Epath.departmentTripHistory)} 
            children={<FaHistory />}/>
            <UserInforButton 
            buttonName='Đổi mật khẩu' 
            onClick={() => history.push(Epath.departmentUserChangePassword)} 
            children={<RiLockPasswordFill />}/>
            <UserInforButton buttonName='Đăng xuất' onClick={() => setShow(true)} children={<BiLogOut />}/>
            <Menu inforActive={true} homeClick={homeClick} carClick={carClick} historyOrderCar={historyOrderCar} historyTrip={historyTrip}/>
            {
                show ?
                <div className={styles.modal_overlay}>
                    <Modal 
                    titleModal={'Bạn có chắc muốn đăng xuất không?'}
                    modalType='danger' 
                    handleCancel={() => setShow(false)}
                    handleOk={onLogOut}
                    />
                </div>
                :
                null
            }
        </div>
    );
};

export default UserInformation;
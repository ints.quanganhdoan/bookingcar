import { Epath } from 'app/routes/routesConfig';
import React from 'react';
import { useHistory } from 'react-router-dom';
import styles from './style.module.scss';

const NotAllowedPage = () => {

    const history = useHistory();

    return (
        <div className={styles.container}>
            <img src="/images/Error/not_allowed.png" alt="not alllowed image" />
            <p>Tài khoản của bạn không thuộc loại tài khoản để truy cập trang này</p>
            <button onClick={() => history.push(Epath.homePage)}>
            Back to Homepage
            </button>
        </div>
    );
};

export default NotAllowedPage;
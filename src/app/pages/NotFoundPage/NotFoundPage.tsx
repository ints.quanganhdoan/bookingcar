import React from 'react';
import styles from './style.module.scss';
import { useHistory } from 'react-router-dom';
import { Epath } from 'app/routes/routesConfig';

type Props = {};

const NotFoundPage = (props: Props) => {

  const history = useHistory();

  return (
    <div className={styles.container}>
      <img src="/images/Error/not_found.png" alt="not found image" />
      <p>Rất tiếc, trang bạn tìm kiếm không tồn tại</p>
      <button onClick={() => history.push(Epath.homePage)}>
        Back to Homepage
      </button>
    </div>
  )
};

export default NotFoundPage;

import styles from './style.module.scss';
import { BsSearch } from "react-icons/bs";

import React from 'react';

const SearchField = ({className}: {className?: string}) => {
    return (
        <div className={`${styles.container} ${className}`}>
            <input className={styles.inputField}/>
            <BsSearch className={styles.iconSearchField}/>
        </div>
    );
};

export default SearchField;
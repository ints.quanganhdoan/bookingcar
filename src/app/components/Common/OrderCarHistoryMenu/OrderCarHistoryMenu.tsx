import React from 'react';
import styles from './style.module.scss';

const OrderCarHistoryMenu = ({setStatus}: {setStatus: any}) => {



    const [activeButton, setActiveButton] = React.useState("Chờ duyệt");
    
    const handleClick = (buttonName: string) => {
        setActiveButton(buttonName);
        setStatus(buttonName);
    }


    return (
        <div className={styles.btn_container}>
            <button 
                onClick={() => handleClick("Chờ duyệt")}
                className={activeButton === "Chờ duyệt" ? styles.active : ""}
            >
                Chờ duyệt
            </button>
            <button 
                onClick={() => handleClick("Đã hủy")}
                className={activeButton === "Đã hủy" ? styles.active : ""}
            >
                Đã hủy
            </button>
            <button 
                onClick={() => handleClick("Chờ hủy")}
                className={activeButton === "Chờ hủy" ? styles.active : ""}
            >
                Chờ hủy
            </button>
            <button 
                onClick={() => handleClick("Bị từ chối")}
                className={activeButton === "Bị từ chối" ? styles.active : ""}
            >
                Bị từ chối
            </button>
        </div>
    );
};

export default OrderCarHistoryMenu;
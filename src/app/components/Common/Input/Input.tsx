import React, { forwardRef } from 'react';
import styles from './style.module.scss';

interface InputType {
    label?: string;
    placeholder?: string;
    className?: string;
    background?: string;
    disabled?: boolean;
    type?: string;
    maxLength?: number;
    format?: string;
}

const Input = forwardRef<HTMLInputElement, InputType>(({
    label, 
    placeholder, 
    className, 
    background, 
    disabled,
    format, 
    type="text",
    ...others
}, ref) => {
    return (
        <div className={`${styles.container} ${className || ''}`}>
            <p>{label}</p>
            <input ref={ref} type={type} disabled={disabled} className={`${styles.inputInfor} ${background || ''}`} placeholder={placeholder} {...others}/>
        </div>
    );
});

export default Input;

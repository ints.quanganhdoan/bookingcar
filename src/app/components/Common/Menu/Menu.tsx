import React from 'react';
import {AiFillCar, AiFillHome} from 'react-icons/ai'
import { BiSolidUser } from 'react-icons/bi';
import styles from './style.module.scss';
import '../../../styles/variables.scss';
import { FaHistory, FaMapMarkerAlt } from 'react-icons/fa';

interface MenuType {
    homeActive?: boolean;
    carActive?: boolean;
    inforActive?: boolean;
    historyOrderCarActive?: boolean;
    historyTripActive?: boolean;
    homeClick?: () => void;
    carClick?: () => void;
    inforClick?: () => void;
    historyOrderCar?: () => void;
    historyTrip?: () => void;
}

const Menu = ({homeActive, carActive, inforActive, historyOrderCarActive, historyTripActive, homeClick, carClick, inforClick, historyOrderCar, historyTrip}: MenuType) => {

    return (
        <div>
            <div className={styles.container}>
                <div className={`${styles.icon_container} ${homeActive ? styles['active-items'] : ''}`}>
                    <AiFillHome 
                    onClick={homeClick} 
                    />
                    <p>Trang chủ</p>
                </div>
                <div className={styles.icon_container}>
                    <AiFillCar 
                    className={carActive ? styles['active-items'] : ''}
                    onClick={carClick} />
                    <p>Đặt xe</p>
                </div>
                <div className={styles.icon_container}>
                    <FaMapMarkerAlt 
                    className={historyOrderCarActive ? styles['active-items'] : ''}
                    onClick={historyOrderCar} />
                    <p>Xe đã đặt</p>
                </div>
                <div className={styles.icon_container}>
                    <FaHistory 
                    className={historyTripActive ? styles['active-items'] : ''}
                    onClick={historyTrip} />
                    <p>Chuyến đi</p>
                </div>
                <div className={styles.icon_container}>
                    <BiSolidUser 
                    className={inforActive ? styles['active-items'] : ''}
                    onClick={inforClick}/>
                    <p>Thông tin</p>
                </div>

            </div> 
        </div>
    );
};

export default Menu;
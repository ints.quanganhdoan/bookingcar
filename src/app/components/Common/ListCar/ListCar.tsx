import React, { useEffect, useState } from 'react';
import styles from './style.module.scss';
import { Link, useHistory } from 'react-router-dom';
import useImageErrorHandling from 'app/helpers/customHook/useImageErrorHandling';
import { Epath } from 'app/routes/routesConfig';

interface VehicleType {
    id_vehicle: number;
    license_plate: string;
    seats: number;
    type: string;
    status_vehicle: string;
    image_vehicle: string;
}

export interface ListCarType {
    avatar: string;
    id_user: number;
    fullname: string;
    vehicle: VehicleType;
}

interface ListCarProps {
    list: ListCarType[];
    numberCars: number;
}


const ListCar: React.FC<ListCarProps> = ({ list, numberCars }) => {

    const handleImageError = useImageErrorHandling('/images/Error/car_default.jpg');

    const history = useHistory();

    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const timer = setTimeout(() => {
            setLoading(false);
        }, 2000); 

        return () => clearTimeout(timer); 
    }, []);


    if(list.length === 0) {
        return(
            <div className={styles.alert}>{loading ? 'Loading...' : 'Không tìm thấy xe nào trong trạng thái này'}</div>
        )
    }

    return (
        <div>
            <div className={styles.wrapper}>
                {
                    list?.slice(0,numberCars).map((item) => (
                        <div className={styles.container} key={item.id_user}>
                            <img 
                            src={item.vehicle.image_vehicle || ''}
                            onError={handleImageError} 
                            alt="car image" />
                            <div className={styles.inforContainer}>
                                <p>{item.vehicle.type}</p>
                                <p>{item.vehicle.license_plate}</p>
                                <p>{item.fullname}</p>
                                <Link to={`${Epath.departmentDriverPage}/${item.id_user}`} className={styles.inforDetails}>Details</Link>
                                <button onClick={() => history.push(`${Epath.departmentDriverPage}/${item.id_user}/order_details`)}>
                                    Đặt xe
                                </button>
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>
    );
};

export default ListCar;
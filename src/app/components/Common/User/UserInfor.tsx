import React from 'react';
import styles from './style.module.scss';
import useImageErrorHandling from 'app/helpers/customHook/useImageErrorHandling';

interface UserInforType {
    departmentRole?: string;
    fullname?: string;
    avatar: string;
    username?: string;
    className?: string;
};

const UserInfor = ({departmentRole, fullname, avatar, username, className}: UserInforType) => {

    const handleImageError = useImageErrorHandling('/images/Error/avatar_default.png');

    return (
        <div className={className}>
            <div className={styles.container}>
                <img 
                src={avatar || ''} 
                alt="avatar" 
                className={styles.avatarImage}
                onError={handleImageError}
                />
                <div>
                    <p>{fullname}</p>
                    <p>{departmentRole}</p>
                    <p>{username}</p>
                </div>
            </div>
        </div>
    );
};

export default UserInfor;
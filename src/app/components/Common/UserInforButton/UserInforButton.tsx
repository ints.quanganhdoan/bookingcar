import React, { ReactNode } from 'react';
import styles from './style.module.scss';

interface ButtonType {
    onClick: () => void;
    buttonName: string;
    children: ReactNode;
}

const UserInforButton = ({onClick, buttonName, children}: ButtonType) => {
    return (
        <div className={styles.btn_container} onClick={onClick}>
            <div className={styles.btn_icon}>{children}</div>
            <div className={styles.btn}>{buttonName}</div>
        </div>
    );
};

export default UserInforButton;
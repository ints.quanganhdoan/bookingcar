import React from 'react';
import styles from './style.module.scss';

interface SelectProps {
    label?: string;
    className?: string;
    background?: string;
    disabled?: boolean;
    options: { label: string; value: string }[];
    placeholder?: string;
    onChange?: () => void;
}

const Select: React.FC<SelectProps> = ({
    label, 
    className, 
    background, 
    disabled,
    options,
    placeholder,
    onChange
}) => {
    return (
        <div className={`${styles.container} ${className || ''}`}>
            <p>{label}</p>
            <select disabled={disabled} className={`${styles.selectInfor} ${background || ''}`} onChange={onChange}>
                <option value="" disabled selected>{placeholder}</option>
                {options.map((option, index) => (
                    <option key={index} value={option.value}>
                        {option.label}
                    </option>
                ))}
            </select>
        </div>
    );
};

export default Select;

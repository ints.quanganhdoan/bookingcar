import React from 'react';
import Input from '../Input/Input';
import { FaRegCalendarAlt } from "react-icons/fa";
import styles from './style.module.scss';

type InputDateType = {
    field: any;
    label: string;
    placeholder: string;
}

const InputDate = ({field, label, placeholder}: InputDateType) => {

    return (
        <div className={styles.container}>
            <label>
                <Input label={label} placeholder={placeholder} type="date" {...field} />
                <FaRegCalendarAlt className={styles.iconDate}/>
            </label>
        </div>
    );
};

export default InputDate;
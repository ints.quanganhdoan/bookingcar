import React from 'react';
import Input from '../Input/Input';
import { IoMdTime } from "react-icons/io";
import styles from './style.module.scss';

type InputTimeType = {
    field: any;
    label: string;
    placeholder: string;
}

const InputTime = ({field, label, placeholder}: InputTimeType) => {

    return (
        <div className={styles.container}>
            <label>
                <Input label={label} placeholder={placeholder} type="time" {...field} />
                <IoMdTime className={styles.iconTime}/>
            </label>
        </div>
    );
};

export default InputTime;
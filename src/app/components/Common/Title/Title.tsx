import React from 'react';
import { IoChevronBack } from 'react-icons/io5';
import styles from './style.module.scss';
import { useHistory } from 'react-router-dom';

const Title = ({titleName}: {titleName: string}) => {

    const history = useHistory();

    return (
        <div className={styles.titleContainer}>
            <IoChevronBack onClick={() => history.go(-1)} className={styles.titleIcon}/>
            <h1 className={styles.title}>{titleName}</h1>
        </div>
    );
};

export default Title;
import React from 'react';
import Input from '../Input/Input';
import { passwordRegex } from 'app/helpers/common';
import { AiFillEye, AiFillEyeInvisible } from 'react-icons/ai';
import styles from './style.module.scss';

interface PasswordType {
    name: string;
    label: string;
    placeholder: string;
    showPassword: boolean;
    register: any;
    errors: any;
    setShowPassword: (value: boolean) => void;
}

const PasswordField = ({name, label, placeholder, showPassword = false, setShowPassword, register, errors}: PasswordType) => {

    return (
        <>
            <div className={styles.passwordField}>
                <Input
                {...register(name, 
                { required: "Password is required",
                  pattern: passwordRegex })}
                label={label}
                aria-invalid={errors[name] ? "true" : "false"}
                type={showPassword ? 'text' : 'password'}
                placeholder={placeholder}
                maxLength={6}
                />
                {showPassword
                ?
                <AiFillEye className={styles.passwordIcon} 
                onClick={() => setShowPassword(!showPassword)}/>
                :
                <AiFillEyeInvisible className={styles.passwordIcon}
                onClick={() => setShowPassword(!showPassword)}/> 
                }
            </div>
        </>
    );
};

export default PasswordField;
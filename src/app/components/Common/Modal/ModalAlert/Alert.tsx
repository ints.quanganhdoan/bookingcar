import React from 'react';

import styles from './style.module.scss';

import { MdOutlineDangerous, MdOutlineDone } from 'react-icons/md';
import { IoWarningOutline } from 'react-icons/io5';
import { IoMdInformationCircleOutline } from 'react-icons/io';
import { AiOutlineLoading3Quarters } from 'react-icons/ai';


interface AlertType {
    contentModal: string;
    typeModal: 'Success' | 'Warning' | 'Fail' | 'Infor' | 'Loading';
}

const showModalIcon = (typeOfModal: string) => {
    let modalIcon = <IoMdInformationCircleOutline />;
    switch(typeOfModal){
        case 'Success': modalIcon = <MdOutlineDone />; break;
        case 'Warning': modalIcon = <IoWarningOutline />; break;
        case 'Fail': modalIcon = <MdOutlineDangerous />; break;
        case 'Infor': modalIcon = <IoMdInformationCircleOutline  />; break;
        case 'Loading': modalIcon = <AiOutlineLoading3Quarters />; break;
    }
    return modalIcon;
}



const showModalTitle = (typeOfModal: string) => {
    let modalMessage = '';
    switch(typeOfModal){
        case 'Success': modalMessage = 'Thành công'; break;
        case 'Warning': modalMessage = 'Cảnh báo'; break;
        case 'Fail': modalMessage = 'Lỗi'; break;
        case 'Infor': modalMessage = 'Thông báo'; break;
        case 'Loading': modalMessage = 'Bạn chờ chút nhé'; break;
    }
    return modalMessage;
}

const ModalColor = (typeOfModal: string) => {
    let modalColor = '';
    switch(typeOfModal){
        case 'Success': modalColor = 'success'; break;
        case 'Warning': modalColor = 'warning'; break;
        case 'Fail': modalColor = 'fail'; break;
        case 'Infor': modalColor = 'infor'; break;
        case 'Loading': modalColor='loading'; break;
    }
    return modalColor;
}



const Alert = ({contentModal, typeModal}: AlertType) => {
    return (
        <div className={`${styles.modal_container} ${styles[ModalColor(typeModal)]}`}>
            {showModalIcon(typeModal)}
            <p>{showModalTitle(typeModal)}</p>
            <p className={typeModal==='Loading' ? styles['loading_content'] : ''}>
                {contentModal}
            </p>
        </div>
    );
};

export default Alert;
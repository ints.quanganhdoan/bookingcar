import React from 'react';
import styles from './style.module.scss';
import Input from '../../Input/Input';

interface ModalType {
    titleModal: string;
    handleCancel: () => void;
    handleOk: () => void;
    modalType?: 'danger' | 'confirm';
    modalInput?: boolean;
    labelInput?: string;
    placeholderInput?: string;
}

const Modal = ({titleModal, handleCancel, handleOk, modalType, modalInput = false, labelInput, placeholderInput}: ModalType) => {
    return (
        <div className={styles.modal_container}>
            <p>{titleModal}</p>
            {
                modalInput === true ?
                <Input label={labelInput} placeholder={placeholderInput} className={modalType === 'danger' ? styles.inputFieldDanger : ''}/>
                :
                null
            }
            <div className={styles.modal_btn}>
                <button onClick={handleCancel}>Hủy bỏ</button>
                <button onClick={handleOk} className={modalType === 'danger' ? styles['danger'] : ''}>Chắc chắn</button>
            </div>
        </div>
    );
};

export default Modal;
import React from 'react';
import { GoPaperAirplane } from "react-icons/go";
import styles from './style.module.scss';

const TripList = ({upcoming}: {upcoming: boolean}) => {
    return (
        <div className={styles.container}>
        <img src="/images/car_test_2.jpg" alt="car image" />
            <div>
                <div>
                {upcoming ? '20/11/2023' : '16/11/2023'}, {upcoming ? '16:00' : '14:00'}
                </div>
            </div>
            <GoPaperAirplane onClick={() => {}}/>
        </div>
    );
};

export default TripList;
import React, { useState } from 'react';
import { MdDeleteOutline } from "react-icons/md";
import styles from './style.module.scss';
import Modal from '../Modal/ModalOption/Modal';
import { listWaitAccessType } from 'store/department/listWaitAccessReducer';
import useImageErrorHandling from 'app/helpers/customHook/useImageErrorHandling';

const ListCarHistory = ({list}: {list: listWaitAccessType[]}) => {

    const[show, setShow]  = useState<boolean>(false);
    const handleImageError = useImageErrorHandling('/images/Error/car_default.jpg');

    return (
        <>
        {
            list.map((item, index) => (
            <div 
            className={`${styles.container} ${index === list.length - 1 ? styles.lastItem : ''}`} 
            key={item.id_booking}
            >
                <img 
                src={item.driver.avatar||''} 
                alt="car image"
                onError={handleImageError} 
                />
                <div className={styles.inforContainer}>
                     <p>{item.driver.fullname}</p>
                     <div>
                         <p>{item.date_start} - {item.date_end}</p>
                         <p>{item.time_start} - {item.time_end}</p>
                     </div>
                     <p>{item.reason_booking}</p>
                 </div>
                 <MdDeleteOutline onClick={() => setShow(true)}/>
             </div>
            ))
        }
        {
            show ?
            <div className={styles.modal_overlay}>
                <Modal 
                titleModal={'Bạn có chắc muốn hủy đặt xe không?'}
                modalType='danger' 
                modalInput={true}
                labelInput='Nhập lí do hủy'
                placeholderInput='Mời bạn nhập lí do hủy đặt xe'
                handleCancel={() => setShow(false)}
                handleOk={() => {}}
                />
            </div>
            :
            null
        }
        </>
    );
};

export default ListCarHistory;
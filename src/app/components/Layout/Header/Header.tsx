import React, { useEffect, useState } from 'react';
import styles from './style.module.scss';

function Header() {



  const [time, setTime] = useState<Date>(new Date());

  useEffect(() => {
    const interval = setInterval(() => {
      setTime(new Date());
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  return(
    <div className={styles.headerNavbar}>
        <div className={styles.headerBasicInfor}>
          <span className={styles.headerTime}>
            {time.getHours()}
            :
            {time.getMinutes() < 10 && '0'}{time.getMinutes()}
          </span>
          <img src="/images/Components/Header/location.svg" alt="location" />
        </div>

        <div className={styles.headerDeviceInfor}>
          <img src="/images/Components/Header/signal.svg" alt="signal" />
          <img src="/images/Components/Header/wifi.svg" alt="wifi" />
          <img src="/images/Components/Header/battery.svg" alt="battery" />
        </div>
    </div>
    ) 
}

export default Header;

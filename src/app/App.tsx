import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { getAuthenticatedFromStorage, getLanguage, getTokenFromLocalStorage, saveLanguage } from './helpers/localStorage';
import RenderRoutes, { routes } from './routes/routes';
import { RootState } from 'types/RootState';
import './styles/variables.scss';
import './styles/animation.scss';
import './styles/base.scss';
import './styles/elements.scss';
import './styles/typography.scss';
import './styles/dependencies/index.scss';

// Set locate to moment lib
const language = getLanguage();
saveLanguage(language);
moment.locale(language);

export function App() {

  let { authenticated } = useSelector(
    (state: RootState) => state.auth,
  );

  if(localStorage.getItem('authenticated')){
    authenticated = !!getAuthenticatedFromStorage();
  }
  

  // const isAuthenticated = !!(authenticated && currentUser && Object.keys(currentUser).length);

  // const { authenticated, currentUser, userRegister } = useSelector(
  //   (state: RootState) => state.auth,
  // );

  // const isAuthenticated = !!(authenticated && currentUser && Object.keys(currentUser).length);

  // useEffect(() => {
  //   if (isAuthenticated) {
  //     dispatch(loadCommonData());
  //   }
  // }, [authenticated, currentUser, isAuthenticated, dispatch]);
  

  return (
    <Router basename="">
      <RenderRoutes routes={routes} isAuthenticated={authenticated} />
    </Router>
  );
}



import { Epath } from "app/routes/routesConfig";
import { useHistory } from "react-router-dom"

export function useMenuDepartmentData() {
    const history = useHistory();
    
    return {
        homeClick: () => history.push(Epath.departmentHomePage),
        carClick: () => history.push(Epath.departmentDriverPage),
        inforClick: () => history.push(Epath.departmentUserInformation),
        historyOrderCar: () => history.push(Epath.departmentOrderDriverHistory),
        historyTrip: () => history.push(Epath.departmentTripHistory)
    };
}